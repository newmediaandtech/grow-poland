<?php
/** 
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'growpl_grow');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pgWvUsj1LuzXFTcEtJZd(6%dV*RDy5Q6XgISsFKQ5IhH0zIx!9tP#cwkNh5mwEK%');
define('SECURE_AUTH_KEY',  'ET&DU)FQp@hcGKUtLOl2PsqcJbfr*FdGQUt*1ZW3woycQoh6xe@44CMVuTefTs@I');
define('LOGGED_IN_KEY',    'l%EE%a5ivQ!TshZrnSeR*@unMPOOaV6vXCVEc!l8WtYkaN9cNdBidQNcrjlFASYx');
define('NONCE_KEY',        '6FzZQh(DWUXHeQq2II8fuG(FBdjyVEVEc&2hVQPXA^mc0sTecu)(zF7qcSLeDBCI');
define('AUTH_SALT',        '33JUsDR73n!RirVwt1ohNDMD4LY92NgW*IBGwaC8rzP#aMOfh1oN1!XwLf2mvT8g');
define('SECURE_AUTH_SALT', '1hp9!Jz8mfH9UOaNyLg789uEd(C@3vJh)c0Mmm2mA4rSj((A6O2tD24bbWZEbMZ6');
define('LOGGED_IN_SALT',   'Kde836g5^z%Rur)Ke#*wapOeZZoV!qOEgSsW&NNlCzaA)6Y*tY&7IgBfzAB1u)oo');
define('NONCE_SALT',       '7Kgs)twN%%59K8kLEN8q@cpLK2zgfF6pC3UFCIC1*omH^r3sFaZ@ujXrzFPYbUug');

define('WP_HOME','http://growpoland.dev');
define('WP_SITEURL','http://growpoland.dev');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each 
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define( 'WP_ALLOW_MULTISITE', true );

define ('FS_METHOD', 'direct');

//--- disable auto upgrade
define( 'AUTOMATIC_UPDATER_DISABLED', true );
?>
