var pckry;
var container;
var countdownimagepath;
var html;

$(window).load(function () {
    //var window_width = $(window).width();
    container = document.querySelector('#container');
    //if (container != undefined) {
    //    pckry = new Packery(container, {
    //        // options
    //        itemSelector: '.item',
    //        gutter: 0,
    //        columnWidth: '.grid-sizer',
    //    });
    //    $("#container-slider").animate({ right: 0 }, 500);        
    //}
    $(".slider").animate({ "right": 0 }, 500);
});

$(function () {
    countdownimagepath = $("[name=countdown-image-path]").val();

    //$("#countdowntimer").countdowntimer({
    //    seconds : '61'
    //});

    var s = $("[name=countdown-seconds]").val();
    if (s > 0)
    {
        UpdateSeconds(s);
    }

    var is_touch_device = 'ontouchstart' in document.documentElement;
    if (is_touch_device) {
        $(".absolute-swipe").addClass("touch-device");
        $(".swipe").swipe({
            allowPageScroll: "vertical",
            swipe: function (event, direction, distance, duration, fingerCount) {
                var next = $("[name=next]").val();
                if (direction == "left" && next != undefined) {
                    window.location = next;
                }
            }
        });
    }

    var iOS = (navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false);
    if (iOS || (navigator.userAgent.indexOf('Safari') != -1 &&
    navigator.userAgent.indexOf('Chrome') == -1)) {
        $("body").addClass("safari");
    }

    var $c = $('#container').imagesLoaded( function() {
        // initialize Packery after all images have loaded
        pckry = $c.packery({
            itemSelector: '.item',
            gutter: 0,
            columnWidth: '.grid-sizer',
        });
    });

    var $cc = $('.contact-container').imagesLoaded(function () {
        // initialize Packery after all images have loaded
        pckry = $cc.packery({
            itemSelector: '.item',
            gutter: 0,
            columnWidth: '.grid-sizer',
        });
    });

    $(".img-cycle").cycle();
    $(".link-cycle").cycle({
        slides: "a"
    });
    $(".Video").colorbox({ iframe: true, innerWidth: 640, innerHeight: 390 });
    $("[name=subscribe_form]").submit(function (e) {
        e.preventDefault();
        $(".errormsg").hide();
        $(".confirmmsg").hide();
        var emailval = $("[name=subscribe_email]").val();
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (emailval != "" && emailReg.test(emailval)) {
            $.post(encodeURI(MVC_baseurl + "Home/Index"), $("[name=subscribe_form]").serialize(), function (data) {
                $("[name=subscribe_email]").val("");
                $(".confirmmsg").html(data).show();
                setTimeout(function () { $(".confirmmsg").hide(); }, 10000);
            });
        } else
        {
            $(".errormsg").show();
            setTimeout(function () { $(".errormsg").hide(); }, 10000);
        }
    });

    $("[name=expert_advice_form]").submit(function (e) {
        e.preventDefault();
        $(".missing").removeClass("missing");
        var b = true;
        $(".required").each(function () {
            $input = $(this);
            if ($input.val() == "")
            {
                b = false;
                $input.addClass("missing");
            }
        });

        var emailval = $("[name=email]").val();
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test(emailval))
        {
            b = false;
            $("[name=email]").addClass("missing");
        }

        var interest_fields = $("[name=interest_fields]:checked").length;
        if (interest_fields == 0)
        {
            b = false;
            $(".required-checkboxes").addClass("missing");
        }
        
        if (b)
        {
            $.post(encodeURI(MVC_baseurl + "ExpertAdvice/Index"), $("[name=expert_advice_form]").serialize(), function (data) {
                $(".form").remove();
                $("#target").html(data);
            });
        }
    });

    $("[name=workbook_form]").submit(function (e) {
        e.preventDefault();
        $(".missing").removeClass("missing");
        var b = true;
        $(".required").each(function () {
            $input = $(this);
            if ($input.val() == "") {
                b = false;
                $input.addClass("missing");
            }
        });

        var emailval = $("[name=email]").val();
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test(emailval)) {
            b = false;
            $("[name=email]").addClass("missing");
        }

        if (b) {
            $.post(encodeURI(MVC_baseurl + "WhatWeDo/Workbook"), $("[name=workbook_form]").serialize(), function (data) {
                window.location = encodeURI(data);
                //alert(data);
            });
        }
    });

    $(".select_news_interest").on("change", function () {
        var val = $(this).val();
        window.location = encodeURI(MVC_baseurl + "News/Index/" + val)
    });
    $(".select_language").on("change", function () {
        var val = $(this).val();
        window.location = encodeURI(val)
    });
    $("[name=filter_form]").on("submit", function (e) {
        e.preventDefault();
        Filter();
    });
    $(".filter").on("change", function () {
        Filter();
    });
    $(".downloadzipsecuresingle").on("click", function (event) {
        event.preventDefault();
        var login = prompt("Please give password: ", "");
        var url = $(this).attr("href");
        var autonummer = $("#autonummer").val();
        $.post(encodeURI(MVC_baseurl + "Pressroom/Login"), { login: login, id: autonummer }, function (json) {
            if (json == "ok") {
                $.post(encodeURI(MVC_baseurl + "Pressroom/DownloadFile"), { id: autonummer }, function (json) {
                    window.location = url;
                });                        
            } else {
                alert("Invalid password.");
            }
        });
    });

    $(".downloadzipsingle").on("click", function (event) {
        event.preventDefault();
        var autonummer = $("#autonummer").val();
        var url = $(this).attr("href");
        $.post(encodeURI(MVC_baseurl + "Pressroom/DownloadFile"), { id: autonummer }, function (json) {
                    window.location = url;
                });
    });

    $(".lightboxlink").each(function () {
        //$(this).colorbox({ html: $("input", $(this)).val(), maxWidth: '90%', maxHeight: '90%' });
        $(this).colorbox({ maxWidth: '90%', maxHeight: '90%' });
    });

    $(".downloadimages").on("click", function () {
        $download = $(this);
        var id = $download.attr('id').substring('3');

        if ($download.hasClass("downloadzipsecure")) {
            var login = prompt("Please give password: ", "");
            var autonummer = $("#autonummer").val();
            $.post(encodeURI(MVC_baseurl + "Pressroom/Login"), { login: login, id: autonummer }, function (json) {
                if (json == "ok") {
                    downloadall(id);
                } else {
                    alert("Invalid password.");
                }
            });
        } else {
            downloadall(id);
        }
    });

    $(".seemore").on("click", function () {
        $link = $(this);
        var id = $link.data("autonummer");
        $link.remove();
        $(".loader").show();
        $.post(encodeURI(MVC_baseurl + "Pressroom/GetMoreImages"), { id: id }, function (data) {
            $("#moreimages").html(data);
            $(".loader").hide();
            $(".lightboxlink").each(function () {
                //$(this).colorbox({ html: $("input", $(this)).val(), maxWidth: '90%', maxHeight: '90%' });
                if ($(this).hasClass("Video")) {
                    $(this).colorbox({ html: $("input", $(this)).val(), maxWidth: '90%', maxHeight: '90%' });
                } else {
                    $(this).colorbox({ maxWidth: '90%', maxHeight: '90%' });
                }
            });
        });
    });

    $(".expand").on("click", function () {
        if ($(".menuposition").hasClass("show")) {
            $(".menuposition").removeClass("show", 1000, "linear");
        } else
        {
            $(".menuposition").addClass("show", 1000, "linear");
        }
    });

    $(".videoshow").on("click", function (e) {
        e.preventDefault();
        var id = $(this).attr("rel");
        $(this).hide();
        $("#video_" + id).show();
        var iframe = document.getElementById("iframe_video_" + id);
        var player = $f(iframe);
        player.api("play");
    });

    $(".video-container").each(function () {
        $container = $(this);
        var id = $container.attr("id");
        $iframe = $container.find("iframe");
        if ($iframe != undefined)
        {
            $iframe.attr("id", "iframe_" + id);
        }
    });

    //$('.menu a').on('click touchend', function (e) {
    //    var el = $(this);
    //    var link = el.attr('href');
    //    AnimateBody(link);
    //});

    $(".menu a").on("click", function (e) {
        e.preventDefault();
        var link = $(this).attr("href");
        AnimateBody(link);
    });

    $(".expand-link").on("click", function () {
        $link = $(this);
        var target = $link.attr("rel");
        var expanded = $link.hasClass("expanded");
        if (expanded) {
            $link.removeClass("expanded");
            $("#" + target).removeClass("visible");
        } else
        {
            $link.addClass("expanded");
            $("#" + target).addClass("visible");
        }       
        
    });
});

function AnimateBody(href)
{
    var windowwidth = -$(window).width() + "px";
    $(".slider").animate({
        "margin-left": "-100%"
    }, 500, function () {
        window.location = href;
        //$(".slider").css("margin-left", 0);
    });
}

function UpdateSeconds(s)
{
    $("#countdown").html(createHtml(s));
    s = s - 1;
    if (s > 0)
    {
        setTimeout(function () {
            $("#countdown-bg").html($("#countdown").html());
        }, 500);
        setTimeout(function () {            
            UpdateSeconds(s);
        }, 1000);
    }
    if (s == 0)
    {
        location.reload();
    }    
}

function createHtml(s)
{
    var return_html = "";
    for (var i = 0, len = s.toString().length; i < len; i++) {
        waarde = s.toString()[i];
        return_html += "<img src='" + countdownimagepath + waarde + ".png' alt='" + waarde + "'/>";
    }
    return_html += "<div class='clear'></div>";
    return return_html;
}

function Filter()
{
    //pckry.destroy();
    $(".loader").show();
    $.post(encodeURI(MVC_baseurl + "Pressroom/Index"), $("[name=filter_form]").serialize(), function (data) {
        $("#container").html(data);
        $(".loader").hide();
        //msnry = new Masonry(container, {
        //    columnWidth: '.grid-sizer',
        //    itemSelector: '.item'
        //});
        pckry = new Packery(container, {
            itemSelector: '.item',
            gutter: 0,
            columnWidth: '.grid-sizer',
        });
    });
}

    function downloadall(id) {
        $.post(encodeURI(MVC_baseurl + "Pressroom/DownloadZip"), { id: id }, function (json) {
            if (json != "error") {
                window.location = json;
            } else {
                alert(json);
            }
        });
    }

    function initialize() {
        var stylez = [
        {
            featureType: "all",
            elementType: "labels",
            stylers: [{ visibility: "off" }]
        }
        ];

        var latlng = new google.maps.LatLng(51.0815354, 4.003171299999963);
        var myOptions = {
            zoom: 2,
            center: latlng
        };
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        map.setOptions({ styles: stylez });
    }

    function drawmarkers() {
        var straat = "";
        var postcode = "";
        var gemeente = "";
        var latlngstr = "";
        var latlng;
        var lat = "";
        var lng = "";
        var latlngbounds = new google.maps.LatLngBounds();
        var kleur = "";

        var icon_link = $("[name=icon_link]").val();

        $(".marker").each(function () {
            var titel = $(this).find(".titel").val();
            var kleur = $(this).find(".kleur").val();
            latlngstr = $(this).find(".latlng").val();
            latlngstr = latlngstr.replace("(", "").replace(")", "");
            lat = latlngstr.split(',')[0];
            lng = latlngstr.split(',')[1];
            latlng = new google.maps.LatLng(lat, lng);
            latlngbounds.extend(latlng);
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: icon_link + kleur + ".png"
            });
            var infowindow = new google.maps.InfoWindow({
                content: "<div style='min-width:200px; min-height:20px;'>" + titel + "</div>"
            });
            google.maps.event.addListener(marker, 'mouseover', function () {
                infowindow.open(map, marker);
            });
            google.maps.event.addListener(marker, 'mouseout', function () {
                infowindow.close(map, marker);
            });
            google.maps.event.addDomListener(window, "resize", function () {
                var center = map.getCenter();
                google.maps.event.trigger(map, "resize");
                map.setCenter(center);
            });
        });
    }