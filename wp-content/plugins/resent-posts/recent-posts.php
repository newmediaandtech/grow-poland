<?php 
/*
Plugin Name: Recent Posts for Grow Poland
Plugin URI http://asttudio.in.ua
Description: Create a carousel with latest person birthdays
Version: 1.0
Author: Sergey Mikhailovsky
License: none
*/



class RecentPost extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'acf_widget', // Base ID
			__( 'Recent Posts Grow ', 'text_domain' ), // Name
			array( 'description' => __( 'Recent Posts Grow', 'text_domain' ), ) // Args
		);
	}

	
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
		
		echo $args['after_widget'];


		$query_args= array('post_type' => 'post', 'posts_per_page' => 2);

		$query_a = new WP_Query($query_args);
		global $current_language;

?> 
<div class="newsfeed nomobile">	
		<?php
		while ($query_a->have_posts()): $query_a-> the_post();?> 
				<div class="newsitem">
		            <h2><?the_title();?></h2>
		            <div class="datum"><? the_date('d F Y'); ?></div>
		            <div><? the_excerpt(); ?></div>
					<div class="interest-fields"><? the_tags('', ', ', '') ?></div>
					<a class="read-more" title="<? the_title() ?>" href="<? the_permalink(); ?>"><?=__('Read more', 'text_domain'); ?> &gt;</a>
				</div>
		 <? endwhile; ?>
</div>
	<?php

	}

	
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'NEWS FEED', 'text_domain' );
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}

} // class Foo_Widget


add_action( 'widgets_init', function(){register_widget( 'RecentPost' );} );


