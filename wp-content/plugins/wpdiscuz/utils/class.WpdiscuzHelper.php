<?php

class WpdiscuzHelper {

    public static $datetime = 'datetime';
    public static $year = 'wc_year_text';
    public static $years = 'wc_year_text_plural';
    public static $month = 'wc_month_text';
    public static $months = 'wc_month_text_plural';
    public static $day = 'wc_day_text';
    public static $days = 'wc_day_text_plural';
    public static $hour = 'wc_hour_text';
    public static $hours = 'wc_hour_text_plural';
    public static $minute = 'wc_minute_text';
    public static $minutes = 'wc_minute_text_plural';
    public static $second = 'wc_second_text';
    public static $seconds = 'wc_second_text_plural';
    private $optionsSerialized;
    private $dbManager;
    public $wcFormAvatar;
    public $wc_allowed_tags = array(
        'br' => array(),
        'a' => array('href' => array(), 'title' => array(), 'target' => array(), 'rel' => array(), 'download' => array(), 'hreflang' => array(), 'media' => array(), 'type' => array()),
        'i' => array(),
        'b' => array(),
        'u' => array(),
        'strong' => array(),
        's' => array(),
        'p' => array('class' => array()),
        'img' => array('src' => array(), 'width' => array(), 'height' => array(), 'alt' => array()),
        'blockquote' => array('cite' => array()),
        'ul' => array(),
        'li' => array(),
        'ol' => array(),
        'code' => array(),
        'em' => array(),
        'abbr' => array('title' => array()),
        'q' => array('cite' => array()),
        'acronym' => array('title' => array()),
        'cite' => array(),
        'strike' => array(),
        'del' => array('datetime' => array()),
    );

    function __construct($wpdiscuzOptionsSerialized, $dbManager) {
        $this->optionsSerialized = $wpdiscuzOptionsSerialized;
        $this->dbManager = $dbManager;
    }

// Set timezone
// Time format is UNIX timestamp or
// PHP strtotime compatible strings
    public function dateDiff($time1, $time2, $precision = 2) {

// If not numeric then convert texts to unix timestamps
        if (!is_int($time1)) {
            $time1 = strtotime($time1);
        }
        if (!is_int($time2)) {
            $time2 = strtotime($time2);
        }

// If time1 is bigger than time2
// Then swap time1 and time2
        if ($time1 > $time2) {
            $ttime = $time1;
            $time1 = $time2;
            $time2 = $ttime;
        }

// Set up intervals and diffs arrays
        $intervals = array(
            $this->optionsSerialized->phrases['wc_year_text']['datetime'][1],
            $this->optionsSerialized->phrases['wc_month_text']['datetime'][1],
            $this->optionsSerialized->phrases['wc_day_text']['datetime'][1],
            $this->optionsSerialized->phrases['wc_hour_text']['datetime'][1],
            $this->optionsSerialized->phrases['wc_minute_text']['datetime'][1],
            $this->optionsSerialized->phrases['wc_second_text']['datetime'][1]
        );
        $diffs = array();
//        exit('ddddd');
// Loop thru all intervals
        foreach ($intervals as $interval) {
// Create temp time from time1 and interval
            $interval = $this->dateComparisionByIndex($interval);
            $ttime = strtotime('+1 ' . $interval, $time1);
// Set initial values
            $add = 1;
            $looped = 0;
// Loop until temp time is smaller than time2
            while ($time2 >= $ttime) {
// Create new temp time from time1 and interval
                $add++;
                $ttime = strtotime("+" . $add . " " . $interval, $time1);
                $looped++;
            }

            $time1 = strtotime("+" . $looped . " " . $interval, $time1);
            $diffs[$interval] = $looped;
        }

        $count = 0;
        $times = array();
// Loop thru all diffs
        foreach ($diffs as $interval => $value) {
            $interval = $this->dateTextByIndex($interval, $value);
// Break if we have needed precission
            if ($count >= $precision) {
                break;
            }
// Add value and interval
// if value is bigger than 0
            if ($value > 0) {
// Add value and interval to times array
                $times[] = $value . " " . $interval;
                $count++;
            }
        }

// Return string with times
        $ago = ($times) ? $this->optionsSerialized->phrases['wc_ago_text'] : $this->optionsSerialized->phrases['wc_right_now_text'];
        return implode(" ", $times) . ' ' . $ago;
    }

    /**
     * get comment author avatar if exists otherwise default avatar
     */
    public function getCommentAuthorAvatar($comment = null) {
        global $current_user;
        get_currentuserinfo();
        $comm_auth_user_email = $current_user->user_email;
        if (function_exists('the_champ_init') && get_user_meta($current_user->ID, 'thechamp_avatar') && is_null($comment)) {
            $comment = (object) array('user_id' => $current_user->ID, 'comment_author_email' => $current_user->user_email, 'comment_type' => '');
        } elseif (!$comment) {
            $comment = $current_user->user_email;
        }
        $comm_auth_avatar = get_avatar($comment, 48);
        if (!$this->wcFormAvatar) {
            $this->wcFormAvatar = $comm_auth_avatar;
        }
        return $comm_auth_avatar;
    }

    public static function initPhraseKeyValue($phrase) {
        $phrase_value = stripslashes($phrase['phrase_value']);
        switch ($phrase['phrase_key']) {
            case WpdiscuzHelper::$year:
                return array(WpdiscuzHelper::$datetime => array($phrase_value, 1));
            case WpdiscuzHelper::$years:
                return array(WpdiscuzHelper::$datetime => array($phrase_value, 1));
            case WpdiscuzHelper::$month:
                return array(WpdiscuzHelper::$datetime => array($phrase_value, 2));
            case WpdiscuzHelper::$months:
                return array(WpdiscuzHelper::$datetime => array($phrase_value, 2));
            case WpdiscuzHelper::$day:
                return array(WpdiscuzHelper::$datetime => array($phrase_value, 3));
            case WpdiscuzHelper::$days:
                return array(WpdiscuzHelper::$datetime => array($phrase_value, 3));
            case WpdiscuzHelper::$hour:
                return array(WpdiscuzHelper::$datetime => array($phrase_value, 4));
            case WpdiscuzHelper::$hours:
                return array(WpdiscuzHelper::$datetime => array($phrase_value, 4));
            case WpdiscuzHelper::$minute:
                return array(WpdiscuzHelper::$datetime => array($phrase_value, 5));
            case WpdiscuzHelper::$minutes:
                return array(WpdiscuzHelper::$datetime => array($phrase_value, 5));
            case WpdiscuzHelper::$second:
                return array(WpdiscuzHelper::$datetime => array($phrase_value, 6));
            case WpdiscuzHelper::$seconds:
                return array(WpdiscuzHelper::$datetime => array($phrase_value, 6));
            default :
                return $phrase_value;
        }
    }

    private function dateComparisionByIndex($index) {
        switch ($index) {
            case 1:
                return 'year';
            case 2:
                return 'month';
            case 3:
                return 'day';
            case 4:
                return 'hour';
            case 5:
                return 'minute';
            case 6:
                return 'second';
        }
    }

    private function dateTextByIndex($index, $value) {
        switch ($index) {
            case 'year':
                return ($value > 1) ? $this->optionsSerialized->phrases['wc_year_text_plural']['datetime'][0] : $this->optionsSerialized->phrases['wc_year_text']['datetime'][0];
            case 'month':
                return ($value > 1) ? $this->optionsSerialized->phrases['wc_month_text_plural']['datetime'][0] : $this->optionsSerialized->phrases['wc_month_text']['datetime'][0];
            case 'day':
                return ($value > 1) ? $this->optionsSerialized->phrases['wc_day_text_plural']['datetime'][0] : $this->optionsSerialized->phrases['wc_day_text']['datetime'][0];
            case 'hour':
                return ($value > 1) ? $this->optionsSerialized->phrases['wc_hour_text_plural']['datetime'][0] : $this->optionsSerialized->phrases['wc_hour_text']['datetime'][0];
            case 'minute':
                return ($value > 1) ? $this->optionsSerialized->phrases['wc_minute_text_plural']['datetime'][0] : $this->optionsSerialized->phrases['wc_minute_text']['datetime'][0];
            case 'second':
                return ($value > 1) ? $this->optionsSerialized->phrases['wc_second_text_plural']['datetime'][0] : $this->optionsSerialized->phrases['wc_second_text']['datetime'][0];
        }
    }

    public static function getArray($array) {
        $new_array = array();
        foreach ($array as $value) {
            $new_array[] = $value[0];
        }
        return $new_array;
    }

    public function makeUrlClickable($matches) {
        $ret = '';
        $url = $matches[2];

        if (empty($url))
            return $matches[0];
        // removed trailing [.,;:] from URL
        if (in_array(substr($url, -1), array('.', ',', ';', ':')) === true) {
            $ret = substr($url, -1);
            $url = substr($url, 0, strlen($url) - 1);
        }
        return $matches[1] . "<a href=\"$url\" rel=\"nofollow\">$url</a>" . $ret;
    }

    public function makeWebFtpClickable($matches) {
        $ret = '';
        $dest = $matches[2];
        $dest = 'http://' . $dest;

        if (empty($dest)) {
            return $matches[0];
        }
        if (in_array(substr($dest, -1), array('.', ',', ';', ':')) === true) {
            $ret = substr($dest, -1);
            $dest = substr($dest, 0, strlen($dest) - 1);
        }
        return $matches[1] . "<a href=\"$dest\" rel=\"nofollow\">$dest</a>" . $ret;
    }

    public function makeEmailClickable($matches) {
        $email = $matches[2] . '@' . $matches[3];
        return $matches[1] . "<a href=\"mailto:$email\">$email</a>";
    }

    public function makeClickable($ret) {
        $ret = ' ' . $ret;
        $ret = preg_replace('#[^\"|\'](https?:\/\/[^\s]+(\.jpe?g|\.png|\.gif|\.bmp))#i', '<a href="$1"><img src="$1" /></a>', $ret);
        // in testing, using arrays here was found to be faster
        $ret = preg_replace_callback('#([\s>])([\w]+?://[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', array(&$this, 'makeUrlClickable'), $ret);
        $ret = preg_replace_callback('#([\s>])((www|ftp)\.[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', array(&$this, 'makeWebFtpClickable'), $ret);
        $ret = preg_replace_callback('#([\s>])([.0-9a-z_+-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,})#i', array(&$this, 'makeEmailClickable'), $ret);
        // this one is not in an array because we need it to run last, for cleanup of accidental links within links
        $ret = preg_replace("#(<a( [^>]+?>|>))<a [^>]+?>([^>]+?)</a></a>#i", "$1$3</a>", $ret);
        $ret = trim($ret);
        return $ret;
    }

    /**
     * check if comment has been posted today or not
     * return boolean
     */
    public static function isPostedToday($comment) {
        return date('Ymd', strtotime(current_time('Ymd'))) <= date('Ymd', strtotime($comment->comment_date));
    }

    /**
     * check if comment is still editable or not
     * return boolean
     */
    public function isCommentEditable($comment) {
        $wc_editable_comment_time = isset($this->optionsSerialized->commentEditableTime) ? $this->optionsSerialized->commentEditableTime : 0;
        return $wc_editable_comment_time && ((time() - strtotime($comment->comment_date_gmt)) < intval($wc_editable_comment_time));
    }

    /**
     * return client real ip
     */
    public function getRealIPAddr() {
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public static function isGuestCanComment() {
        $user_can_comment = TRUE;
        if (get_option('comment_registration')) {
            if (!is_user_logged_in()) {
                $user_can_comment = FALSE;
            }
        }
        return $user_can_comment;
    }

    public function formBuilder($is_main, $unique_id, $commentsCount) {
        global $post, $current_user;
        get_currentuserinfo();

        $wc_is_name_field_required = ($this->optionsSerialized->isNameFieldRequired) ? 'required="required"' : '';
        $wc_is_email_field_required = ($this->optionsSerialized->isEmailFieldRequired) ? 'required="required"' : '';

        if (!$is_main || $commentsCount) {
            $textarea_placeholder = $this->optionsSerialized->phrases['wc_comment_join_text'];
        } else {
            $textarea_placeholder = $this->optionsSerialized->phrases['wc_comment_start_text'];
        }
        $validateCommentlength = (intval($this->optionsSerialized->commentTextMinLength) && intval($this->optionsSerialized->commentTextMaxLength)) ? 'data-validate-length-range="' . $this->optionsSerialized->commentTextMinLength . ',' . $this->optionsSerialized->commentTextMaxLength . '"' : '';
        ?>
        <div class="wc-form-wrapper <?php echo!$is_main ? 'wc-secondary-form-wrapper' : 'wc-main-form-wrapper'; ?>"  <?php echo!$is_main ? "id='wc-secondary-form-wrapper-$unique_id'  style='display: none;'" : "id='wc-main-form-wrapper-$unique_id'"; ?> >
            <div class="wpdiscuz-comment-message" style="display: block;"></div>
            <?php if (!$is_main) { ?>
                <div class="wc-secondary-forms-social-content"></div>
            <?php } ?>
            <?php
            if (WpdiscuzHelper::isGuestCanComment()) {
                ?>
                <form class="wc_comm_form <?php echo!$is_main ? 'wc-secondary-form-wrapper' : 'wc_main_comm_form'; ?>" method="post" action="">
                    <div class="wc-field-comment">

                        <?php if ($this->optionsSerialized->wordpressShowAvatars) { ?>
                            <div class="wc-field-avatararea">
                                <?php echo $this->getCommentAuthorAvatar(); ?>
                            </div>
                        <?php } ?>
                        <div class="wpdiscuz-item wc-field-textarea" <?php
                        if (!$this->optionsSerialized->wordpressShowAvatars) {
                            echo ' style="margin-left: 0;"';
                        }
                        ?>>
                            <textarea <?php echo $validateCommentlength; ?> placeholder="<?php echo $textarea_placeholder; ?>" required="required" name="wc_comment" class="wc_comment wc_field_input"></textarea>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="wc-form-footer"  style="display: none;">
                        <?php if (!is_user_logged_in()) { ?>
                            <div class="wc-author-data">
                                <div class="wc-field-name wpdiscuz-item">
                                    <input type="text" placeholder="<?php echo $this->optionsSerialized->phrases['wc_name_text']; ?>" value=""  <?php echo $wc_is_name_field_required; ?> name="wc_name" class="wc_name wc_field_input" >
                                </div>
                                <div class="wc-field-email wpdiscuz-item">
                                    <input type="email" placeholder="<?php echo $this->optionsSerialized->phrases['wc_email_text']; ?>" value="" <?php echo $wc_is_email_field_required; ?> name="wc_email"  class="wc_email wc_field_input email">
                                </div>
                                <div style="clear:both"></div>
                            </div>
                        <?php } ?>
                        <div class="wc-form-submit">
                            <?php
                            $wpdiscuz_is_captcha_members = $this->optionsSerialized->captchaShowHideForMembers && is_user_logged_in();
                            if (!$this->optionsSerialized->captchaShowHide) {
                                if ($wpdiscuz_is_captcha_members || !is_user_logged_in()) {
                                    ?>
                                    <div class="wc-field-captcha wpdiscuz-item">
                                        <input type="text" maxlength="5" value="" required="required" name="wc_captcha"  class="wc_field_input wc_field_captcha">
                                        <span class="wc-label wc-captcha-label">
                                            <a class="wpdiscuz-nofollow" href="#" rel="nofollow">
                                                <?php $wc_captcha_img_url = admin_url('admin-ajax.php') . '?action=generateCaptcha&wpdiscuz_captcha_id=' . $unique_id; ?>
                                                <img class="wc_captcha_img" src="<?php echo wp_nonce_url($wc_captcha_img_url, 'wc_captcha_img'); ?>&wpdiscuz-rand=<?php echo md5($unique_id); ?>">
                                            </a>
                                            <a class="wpdiscuz-nofollow wc_captcha_refresh_img" href="#" rel="nofollow">
                                                <img  class=""   src="<?php echo plugins_url(WpdiscuzCore::$PLUGIN_DIRECTORY . '/assets/img/captcha-loading.png') ?>">
                                            </a>
                                        </span>
                                        <span class="captcha_msg"><?php echo $this->optionsSerialized->phrases['wc_captcha_text']; ?></span>
                                        <div class="alert"><?php echo $this->optionsSerialized->phrases['wc_msg_captcha_expired']; ?></div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                            <div class="wc-field-submit">
                                <?php if (!is_user_logged_in() && $this->optionsSerialized->weburlShowHide) { ?>
                                    <div class="wc-field-website wpdiscuz-item">
                                        <input type="url" placeholder="<?php echo $this->optionsSerialized->phrases['wc_website_text']; ?>" value="" name="wc_website" class="wc_website wc_field_input">
                                    </div>
                                <?php } ?>
                                <?php if ($this->optionsSerialized->wordpressThreadComments || class_exists('Prompt_Comment_Form_Handling')) { ?>
                                    <div class="wc_notification_checkboxes" style="display:block">
                                        <?php
                                        if (class_exists('Prompt_Comment_Form_Handling') && $this->optionsSerialized->usePostmaticForCommentNotification) {
                                            ?>
                                            <input id="wc_notification_new_comment-<?php echo $unique_id; ?>" class="wc_notification_new_comment-<?php echo $unique_id; ?>" value="post"  type="checkbox" name="wpdiscuz_notification_type"/> <label class="wc-label-comment-notify" for="wc_notification_new_comment-<?php echo $unique_id; ?>"><?php _e('Participate in this discussion via email', 'wpdiscuz'); ?></label><br />
                                            <?php
                                        } elseif ($this->optionsSerialized->showHideReplyCheckbox) {
                                            $wpdiscuz_subscription_type = '';
                                            if ($current_user->ID) {
                                                $wpdiscuz_subscription_type = $this->dbManager->hasSubscription($post->ID, $current_user->user_email);
                                            }
                                            if (!$wpdiscuz_subscription_type) {
                                                ?>
                                                <input id="wc_notification_new_comment-<?php echo $unique_id; ?>" class="wc_notification_new_comment-<?php echo $unique_id; ?>" value="comment"  type="checkbox" name="wpdiscuz_notification_type"/> <label class="wc-label-comment-notify" for="wc_notification_new_comment-<?php echo $unique_id; ?>"><?php echo $this->optionsSerialized->phrases['wc_notify_on_new_reply']; ?></label><br />
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                <?php } ?>
                                <input type="button" class="wc_comm_submit button alt"  value="<?php echo $this->optionsSerialized->phrases['wc_submit_text']; ?>" name="submit">
                            </div>
                            <div style="clear:both"></div>
                        </div>

                    </div>
                    <?php wp_nonce_field('wpdiscuz_comment_form_nonce_action', 'wpdiscuz_comment_form_nonce'); ?>
                    <input type="hidden"  value="<?php echo $unique_id; ?>" name="wpdiscuz_unique_id">
                </form>
            <?php } else { ?>
                <p class="wc-must-login">
                    <?php
                    echo $this->optionsSerialized->phrases['wc_you_must_be_text'];
                    $login = wp_loginout(get_permalink(), false);
                    $login = preg_replace('!>([^<]+)!is', '>' . $this->optionsSerialized->phrases['wc_logged_in_text'], $login);
                    echo ' ' . $login . ' ' . $this->optionsSerialized->phrases['wc_to_post_comment_text'];
                    ?>
                </p>
                <?php
            }
            ?>
        </div>
        <?php
    }

    public function getUIDData($uid) {
        $id_strings = explode('_', $uid);
        return $id_strings;
    }

    /*
     * register new session
     */

    public function registerSession() {
        if (!session_id() && !$this->optionsSerialized->captchaShowHide) {
            if ((is_user_logged_in() && $this->optionsSerialized->captchaShowHideForMembers) || !is_user_logged_in()) {
                @session_start();
            }
        }
    }

    public function generateCaptcha() {
        if (isset($_GET['_wpnonce']) && wp_verify_nonce($_GET['_wpnonce'], 'wc_captcha_img') && isset($_GET['wpdiscuz_captcha_id']) && trim($_GET['wpdiscuz_captcha_id'])) {
            $comm_id = trim($_GET['wpdiscuz_captcha_id']);
            include_once 'captcha/captcha.php';
        }
    }

    public function setCaptchaExpired() {
        if (isset($_SESSION['wc_captcha']) && isset($_POST['wpdiscuzAjaxData']) && trim($_POST['wpdiscuzAjaxData'])) {
            $comm_id = trim($_POST['wpdiscuzAjaxData']);
            $_SESSION['wc_captcha'][$comm_id] = md5(uniqid());
        }
        wp_die();
    }

    public function isShowLoadMore($parentId, $args) {
        $postId = $args['post_id'];
        $postAllParent = $this->dbManager->getAllParentCommentCount($postId, $this->optionsSerialized->wordpressThreadComments);
        $showLoadeMore = false;
        if ($postAllParent) {
            if ($args['orderby'] == 'comment_date_gmt') {
                if ($args['order'] == 'desc' && $parentId) {
                    $minId = min($postAllParent);
                    $showLoadeMore = $minId < $parentId;
                } else {
                    $maxId = max($postAllParent);
                    $showLoadeMore = $maxId > $parentId;
                }
                $showLoadeMore = $showLoadeMore && $this->optionsSerialized->wordpressCommentPerPage && (count($postAllParent) > $this->optionsSerialized->wordpressCommentPerPage);
            } else {
                if ($this->optionsSerialized->loadAllComments && $args['limit'] == 0) {
                    $showLoadeMore = false;
                } else {
                    $showLoadeMore = $args['offset'] + $this->optionsSerialized->wordpressCommentPerPage < count($postAllParent);
                }
            }
        }
        return $showLoadeMore;
    }

    public function superSocializerFix() {
        if (function_exists('the_champ_login_button')) {
            ?>
            <div id="comments" style="width: 0;height: 0;clear: both;margin: 0;padding: 0;"></div>
            <div id="respond" class="comments-area">
            <?php } else { ?>
                <div id="comments" class="comments-area">
                    <div id="respond" style="width: 0;height: 0;clear: both;margin: 0;padding: 0;"></div>
                    <?php
                }
            }

        }
        