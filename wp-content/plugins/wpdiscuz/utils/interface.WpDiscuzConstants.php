<?php

interface WpDiscuzConstants {

    const OPTION_SLUG_OPTIONS                         = 'wc_options';
    const OPTION_SLUG_VERSION                         = 'wc_plugin_version';
    const OPTION_SLUG_OPTIMIZED_LAST_COMMENT_ID       = 'wpdiscuz_optimized_last_comment_id';
    const META_KEY_CHILDREN                           = 'wpdiscuz_child_ids';
    const META_KEY_VOTES                              = 'wpdiscuz_votes';
    const JS_MAIN_FORM_TIME_OUT                       = 30;
    const JS_SECONDARY_FORM_TIME_OUT                  = 15;
    const LOAD_ACTION_DEFAULT                         = 0;
    const LOAD_ACTION_REST                            = 1;
    const LOAD_ACTION_VOTE                            = 2;
    const SUBSCRIPTION_POST                           = 'post';
    const SUBSCRIPTION_ALL_COMMENT                    = 'all_comment';
    const SUBSCRIPTION_COMMENT                        = 'comment';

}
