<?php
/*
*Template Name:Portfolio Page
*/

get_header(); 

$args = array( 'post_type' => 'portfolios',  'orderby' => 'rand');
$loop = new WP_Query( $args );
?>
<div class="super-container">
        <div class="slider">
            <div class="wrapper mainwrapper">
                <div id="FullContent"></div>
                <div class="left one-third">
                    <?php if ( is_active_sidebar( 'left_portfolio' ) ) : ?>
	<div id="sidebar-left-portfolio">
		<?php dynamic_sidebar( 'left_portfolio' ); ?>
	</div>
<?php endif; ?>
                </div>
                <div class="right two-thirds">
    <div id="container-position">
        <div id="container-slider">
            <div id="container">
                <div class="grid-sizer"></div>
                <?php  
                while ( $loop->have_posts() ) : $loop->the_post(); $medium_bool = get_field('big_tile_image') ?>
                	<div class="item <?php if ($medium_bool ) {echo 'medium'; } else {echo 'small';} ?>">
                		<div class="cycle">
                			<a class="no-decoration Item" href="<? the_permalink();?>">
								<?php 

$image = get_field('front_page_image');

if( !empty($image) ): ?>

	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	<?php $image = NULL;?>

<?php endif; ?>
								<span class="item-overlay">
				                                <span class="portfolio-title" style="text-decoration:none !important;">
				                                    <?the_title();?>
				                                </span>
				                                <span class="item-overlay-bg"></span>
				                </span>
                			</a>
                		</div>
                	</div>
				<?php endwhile; ?>

            </div>
                    </div>
                </div>
                
	        <?
	switch (substr(get_bloginfo( 'language' ), 0, 2)) {
		case 'en':
			echo '<a class="clientlist" href="/en/Clientlist">Client list</a>';
			break;
		
		default:
			echo '<a class="clientlist" href="/pl/Clientlist">Lista klientów</a>';	
			break;
	}
	        ?>
            </div>
        
                	<div class="clear"></div>
        </div>
        
        <div class="push"></div>
    </div>
</div>
<?php get_footer(); ?>