<?php
/*
*Template Name:Contacts Page
*/

get_header(); 


?>
<div class="super-container">
    <div class="slider">
        <div class="wrapper mainwrapper">
            <div class="left one-third">
                <h1><?the_title()?></h1>
                <div class="nomobile cycle link-cycle" style="position: relative;">
                    <a class="cursor cycle-slide cycle-sentinel" href="javascript:void(0);" target="_self" >
                        <? $thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
$thumb_url = $thumb_url_array[0]; ?>
                        <img src="
                            <?=$thumb_url?>" alt="">
                        </a>
                        <a class="cursor cycle-slide cycle-slide-active" href="javascript:void(0);" target="_self">
                            <img src="
                                <?=$thumb_url?>" alt="">
                            </a>
                            
                        </div>      
                        <?php if ( is_active_sidebar( 'contacts' ) ) : ?>
                               <div id="sidebar-left-contacts">
                                  <?php dynamic_sidebar( 'contacts' ); ?>
                               </div>
                            <?php endif; ?>               
                    </div>
                    <div class="right two-thirds">
                        <article class="persons">
                            <?the_content();?>
                        </article>
                    </div>

            <div class="clear"></div>
        </div>
        <div class="push"></div>
    </div>
</div>
<?php get_footer(); ?>