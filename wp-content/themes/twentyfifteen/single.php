<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */


get_header(); 
$category = get_the_category();
// echo '<pre>';
$category_slug = $category[0]->slug;
// $args = array( 'post_type' => 'post');
// $loop = new WP_Query( $args );
?>
<div class="super-container">
        <div class="slider">
            <div class="wrapper mainwrapper">
            <div class="left one-third">
            <? if($category_slug){
            
                 get_template_part( 'sidebar', $category_slug );
            }
            ?>
             </div>   
                <div class="right two-thirds">
    <div class="news-wrapper">
                    <?php while ( have_posts() ) : the_post(); ?>
                        <div class="thumb-single">  
                                    <?php twentyfifteen_post_thumbnail(); ?>
                        </div>
                                    <?php the_date('d F Y', '<h2>', '</h2>'); ?>
                                    <h2 class="title"><?the_title()?></h2>
                                    <article class="content"><?the_content()?></article>
                                    <div class="add_50_pixels"></div>
                                    <?php if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;

            // Previous/next post navigation.
            // the_post_navigation( array(
            //     'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'twentyfifteen' ) . '</span> ' .
            //         '<span class="screen-reader-text">' . __( 'Next post:', 'twentyfifteen' ) . '</span> ' .
            //         '<span class="post-title">%title</span>',
            //     'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'twentyfifteen' ) . '</span> ' .
            //         '<span class="screen-reader-text">' . __( 'Previous post:', 'twentyfifteen' ) . '</span> ' .
            //         '<span class="post-title">%title</span>',
            // ) );
                 endwhile; ?>


                  <div class="news-share-container addthiss_bottom">
    <div class="mobile-splitter"></div>
    <div class="addthis_toolbox addthis_default_style" addthis:url="<?the_permalink()?>" addthis:title="<?the_title()?>">
        <a class="addthis_button_email" target="_blank" title="E-mail" href="#">
            <span>E-mail</span>
            <img src="/Content/pictures/share_email_grey.png" alt="e-mail">
        </a>
        <a href="<?the_permalink()?>" target="_blank">
            <span>Permalink</span>
            <img src="/Content/pictures/share_permalink_grey.png" alt="permalink">
        </a>
        <a class="addthis_button_twitter" title="Tweet" href="#">
            <span>Tweet</span>
            <img src="/Content/pictures/share_twitter_grey.png" alt="tweet">
        </a>
        <a class="addthis_button_facebook" title="Facebook" href="#">
            <span>Share</span>
            <img src="/Content/pictures/share_facebook_grey.png" alt="share">
        </a>
        <a class="addthis_button_linkedin no-border" target="_blank" title="LinkedIn" href="#">
            <span>Share</span>
            <img src="/Content/pictures/share_linkedin_grey.png" alt="share">
        </a>
    <div class="atclear"></div></div>
</div>
                        </div>
                </div> 

            </div>





















                	<div class="clear"></div>
        <div class="push"></div>
    </div>
</div>
<?php get_footer(); ?>