<?php
/**
*Template Name:What We Do Page
 */

get_header(); ?>

<div class="super-container">
        <div class="slider">
            <div class="wrapper mainwrapper">
                
                <div class="right one-third">
                    <?php if ( is_active_sidebar( 'what_we_do' ) ) : ?>
					<div id="sidebar-left-what-we-do">
							<?php dynamic_sidebar( 'what_we_do' ); ?>
					</div>
					<?php endif; ?>
                </div>
                <div class="left two-thirds">

                <?php
		// Start the loop.
		while ( have_posts() ) : the_post();?>

			<article class="entry-content">
		<?php the_content(); ?>

			</article>
		<?php endwhile;
		?>
                </div>
        </div>
    	<div class="clear"></div>
        <div class="push"></div>
        </div>
</div>


<?php get_footer(); ?>
