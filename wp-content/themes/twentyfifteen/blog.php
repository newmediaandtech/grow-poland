<?php
/*
*Template Name:Blog Page
*/

get_header(); 

$args = array( 'post_type' => 'post', 'category_name' => 'blog', 'posts_per_page' => 30, 'orderby' => 'rand');
$loop = new WP_Query( $args );
?>
<div class="super-container">
    <div class="slider">
        <div class="wrapper mainwrapper">

            <div class="left one-third">
                <?php if ( is_active_sidebar( 'left_blog' ) ) : ?>
                    <div id="sidebar-left-blog">
                        <?php dynamic_sidebar( 'left_blog' ); ?>
                    </div>
                <?php endif; ?>
                <div class="newsfeed twittertop">
                    <div class="newsitem">
                        <? if (substr(get_bloginfo( 'language' ), 0, 2) == 'pl'):?>
                        <h2>OBSERWUJ NAS NA TWITTERZE</h2>
                    <? else:?>
                    <h2>FOLLOW US ON TWITTER</h2>
                    <?endif;?>
                </div>
            </div>
            <div class="twitterfeed">

                <a class="twitter-timeline" height="500" data-dnt="true"  href="https://twitter.com/BBN_B2B" data-widget-id="669105020672061440" data-chrome="nofooter transparent noheader" data-lang="<?=substr(get_bloginfo( 'language' ), 0, 2);?>">Tweets door @BBN_B2B</a>



            </div>
        </div>
        <div class="right two-thirds">
            <div class="blog-wrapper">
                <div id="container-position">
                    <div id="container-slider">
                        <div id="container">
                            <div class="grid-sizer"></div>
                            <?php 
                            while ( $loop->have_posts() ) : $loop->the_post(); ?>
                            <div class="item medium blog auto-height">
                                <div class="item-wrapper-gp">   
                                    <a class="press-link" href="<?the_permalink();?>">
                                        <? $thumb_id = get_post_thumbnail_id();
                                        $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
                                        $thumb_url = $thumb_url_array[0]; ?>

                                        <img src="<?php echo $thumb_url; ?>" />
                                        <?php $image = NULL;?>
                                    </a>
                                    <div class="contactinfo pressinfo bloginfo">
                                        <a class="press-link" href="<?the_permalink();?>">
                                            <span class="naam">
                                                <? the_title(); ?>
                                            </span>
                                            <span class="contactbeschrijving">
                                                <? the_excerpt();?>
                                            </span>
                                        </a>

                                        <div class="interest-fields">
                                            <?php the_tags( '', ' • ', '' ); ?>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>


            <?php  the_posts_pagination( array(
                'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
                'next_text'          => __( 'Next page', 'twentyfifteen' ),
                'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
                ) );
                ?>
            </div>
        </div> 
    </div>
    <div class="clear"></div>
    <div class="clear"></div>
    <div class="push"></div>
</div>
</div>
<script type="text/javascript">!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = "//platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");</script>
<?php get_footer(); ?>