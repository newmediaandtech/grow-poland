<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	</div><!-- .site-content -->

<div class="footer-container ">
        <div class="footer">
            <div class="wrapper">
                <div class="contentmargin">
                    <div class="left partners-wrapper">
                        <span class="partners spacing">Partners</span>

                        <?php if ( is_active_sidebar( 'footer_logos_section' ) ) : ?>
                       <div id="footer_logos_section">
                          <?php dynamic_sidebar( 'footer_logos_section' ); ?>
                       </div>
                    <?php endif; ?>


                        <div class="clear"></div>
                    </div>
                    
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="bottom">
            <div class="wrapper">
                <div class="contentmargin">

                    <a href="/en/TermsAndConditions">Disclaimer and terms of use</a>
                    <span class="bottomspacer">.</span>


                    <a href="mailto:info@growpoland.pl">info@growpoland.pl</a>
                    <span class="bottomspacer">.</span>
                    Warsaw Financial Centre
                     <span class="bottomspacer">.</span>
                    53 Emilii Plater Street, 11 floor
                     <span class="bottomspacer">.</span>
                    00-113 Warsaw
                    <span class="bottomspacer">.</span>
                    Poland
                    <span class="bottomspacer">.</span>
                    T +48 22 528 93 82
                    <span class="bottomspacer">.</span>
                    F +48 22 528 67 01
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>

</div><!-- .site -->

<?php wp_footer(); ?>
<script type="text/javascript">

    $("div.header").css({top: $("div.cookie-wrapper").height()});
    $("div.super-container>div.slider").css({'margin-top': $("div.cookie-wrapper").height()});

    $(".close-cookies").on("click", function (e) {
        e.preventDefault();
var dateP = new Date(new Date().getTime() + 6000000 * 1000);
document.cookie = "growpolandPricvacyPolicy=true; path=/; expires=" + dateP.toUTCString();
                $(".cookie-wrapper").remove();
    $("div.header").css({top: 0});
    $("div.super-container>div.slider").css({'margin-top': '0px'});



    });
    if ($('#menu-lang').hasClass('en')) {
        $('.read-more').text('Read more >');
        console.log('en');
        $('.elp-widget h2.widgettitle').text('Stay in touch');
        $('.es_caption').text('Subscribe to receive the official GROW newsletters.');
        $('.sharebuttons>ul.sharemenu>li:nth-child(1)>ul>li:nth-child(1)>a').text('Share grow on twitter');
        $('.sharebuttons>ul.sharemenu>li:nth-child(1)>ul>li:nth-child(2)>a').text('Visit grow on twitter');
        $('.sharebuttons>ul.sharemenu>li:nth-child(2)>ul>li:nth-child(1)>a').text('Share grow on facebook');
        $('.sharebuttons>ul.sharemenu>li:nth-child(2)>ul>li:nth-child(2)>a').text('Visit grow on facebook');
        $('.sharebuttons>ul.sharemenu>li:nth-child(3)>ul>li:nth-child(1)>a').text('Share grow on linkedin');
        $('.sharebuttons>ul.sharemenu>li:nth-child(3)>ul>li:nth-child(2)>a').text('Visit grow on linkedin');
        $('.sharebuttons>ul.sharemenu>li:nth-child(4)>ul>li:nth-child(1)>a').text('VISIT GROW ON YOUTUBE');


    }
    else {
        $('.read-more').text('Więcej >');
        $('.es_textbox_button').val('SUBSKRYBUJ');
}

$(".es_lablebox").text('E-mail:');
$('div.right.two-thirds .newscontainer img').parent('a').colorbox({width: '60%', rel:'gal'});
$("._5lm5._2pi3._3-8y").remove();
    </script>



</body>
</html>
