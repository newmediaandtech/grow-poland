
<img class="logo" src="http://growpoland.pl/Content/pictures/logo.png" alt="Ark - Strategic marketing &amp; communication">

<p style="font-size: 12px;padding-top: 50px ">THANK YOU FOR YOUR INTEREST IN OUR THE BRAND JOURNEY</p>
<p style="font-size: 12px; padding-bottom: 50px">Please click here to download your copy of THE BRAND JOURNEY</p>
<div class="footer" style="padding: 26px 0 30px 0; background: #3D3D3F; color: #ffffff;
">
<span class="partners spacing" style=" letter-spacing: 0.07em;  display: block;
    margin-bottom: 8px; padding-left: 3%">PARTNERS</span>


<table>
    <tr>
        <td style="padding-left:10%">
<a href="http://www.bbn-international.com" target="_blank">
    <img class="footerlogo blocklogo bbn_logo"
         src="http://growpoland.pl/wp-content/uploads/2015/11/BBN_New_Logo.png"
         alt="BBN ">
</a>
        </td>
        <td style="padding-left:10%">
            <a href="http://www.eloqua.com/" target="_blank">
                <img class="footerlogo blocklogo"
                     src="http://growpoland.pl/Content/pictures/logo_oracle.png"
                     alt="Oracle Eloqua" style="  display: block;margin-right: 0px;  max-height: 46px;
    margin-bottom: 10px;
    vertical-align: top;">
            </a>

    </tr>
    <tr>
        <td style="padding-left:10%">
            <a href="http://idbbn.com" target="_blank">
                <img class="footerlogo blocklogo idbbn_logo"
                     src="http://growpoland.pl/Content/pictures/IDBBN_logo_vaaka.png"
                     alt="IDBBN vaaka" style="  display: block;margin-right: 0px;  max-height: 46px;
    margin-bottom: 10px;
    vertical-align: top;">
            </a>
        </td>
        <td style="padding-left:10%">
            <a href="http://www.epcreatives.pl" target="_blank">
                <img class="footerlogo blocklogo pelka_logo"
                     src="http://growpoland.pl/Content/pictures/Pelka360_logo.png"
                     alt="Pelka360 " style="  display: block;margin-right: 0px;  max-height: 46px;
    margin-bottom: 10px;
    vertical-align: top;">
            </a>
        </td>
        <td style="padding-left:10%">
            <a href="http://www.ark.be" target="_blank">
                <img class="footerlogo blocklogo ark_logo"
                     src="http://growpoland.pl/wp-content/uploads/2015/11/ark.png"
                     alt="ARK " style="  display: block;margin-right: 0px;  max-height: 46px;
    margin-bottom: 10px;
    vertical-align: top;">
            </a>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="font-size: 10px; text-align: center; color: #B1B3B4">
            <a href="http://growpoland.pl/en/TermsAndConditions" style=" color: #B1B3B4;
    text-decoration: none; ">Disclaimer and terms of use . </a>  <a href="mailto:info@growpoland.pl" style=" color: #B1B3B4;
    text-decoration: none;">info@growpoland.pl</a>
            </td>
        </tr>
    <tr>
        <td colspan="3" style="font-size: 10px; text-align: center; color: #B1B3B4">
            Warsaw Financial Centre . 53 Emilii Plater Street, 11 floor . 00-113 Warsaw . Poland . T +48 22 528 93 82 . F +48 22 528 67 01
    </td>
    </tr>

</div>
