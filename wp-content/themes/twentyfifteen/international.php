<?php
/*
*Template Name:International Page
*/

get_header(); 


?>
<div class="super-container">
        <div class="slider">


<div class="wrapper mainwrapper">
                
                <div class="left one-third">
                    
    <h1>International</h1>
    
<div class="nomobile cycle link-cycle" style="position: relative;">
<a class="cursor cycle-slide cycle-sentinel" href="javascript:void(0);" target="_self" >
        <? $thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
$thumb_url = $thumb_url_array[0]; ?>
        <img src="<?=$thumb_url?>" alt="">
    </a>
    
    
    
<a class="cursor cycle-slide cycle-slide-active" href="javascript:void(0);" target="_self">
        <img src="<?=$thumb_url?>" alt="">
        
    </a></div>

    <div class="newsfeed">
        
<div class="detailitem">
<?the_content();?>

</div>

    </div>

                </div>
                <div class="right two-thirds">
                    

    <input type="hidden" name="icon_link" value="/Content/pictures/icon_">
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN Belgium, Rupelmonde">
        <input type="hidden" class="latlng" value="(51.1255599, 4.271306600000003)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN Canada, Toronto">
        <input type="hidden" class="latlng" value="(43.653597, -79.383714)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN Czech Republic, Brno">
        <input type="hidden" class="latlng" value="(49.194667, 16.610061)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN Czech Republic, Prague">
        <input type="hidden" class="latlng" value="(50.085714, 14.424114)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN Czech Republic, Prostejov">
        <input type="hidden" class="latlng" value="(49.471858, 17.111856)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN Denmark, Copenhagen">
        <input type="hidden" class="latlng" value="(55.681237, 12.587961)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN Netherlands, Oisterwijk">
        <input type="hidden" class="latlng" value="(51.578975, 5.188400)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN Finland, Turku">
        <input type="hidden" class="latlng" value="(60.450520, 22.265053)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN Germany, Viernheim">
        <input type="hidden" class="latlng" value="(49.541990, 8.578598)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN Germany, Munich">
        <input type="hidden" class="latlng" value="(48.138445, 11.575450)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN Germany, Stuttgart">
        <input type="hidden" class="latlng" value="(48.774514, 9.175756)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN United Arab Emirates, Dubai">
        <input type="hidden" class="latlng" value="(25.203153, 55.283746)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN United Arab Emirates, Abu Dhabi">
        <input type="hidden" class="latlng" value="(24.480946, 54.372962)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN Russia, Moscow">
        <input type="hidden" class="latlng" value="(55.753850, 37.623569)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN Singapore">
        <input type="hidden" class="latlng" value="(1.298999, 103.858908)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN Sweden, Stockholm">
        <input type="hidden" class="latlng" value="(59.329207, 18.068055)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN United Kingdom, Aberdeen">
        <input type="hidden" class="latlng" value="(57.149785, -2.094408)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN USA, Wilwaukee">
        <input type="hidden" class="latlng" value="(43.031546, -87.909065)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN USA, Chicago">
        <input type="hidden" class="latlng" value="(41.874776, -87.624009)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN USA, Lincoln">
        <input type="hidden" class="latlng" value="(40.813549, -96.674154)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN USA, Houston">
        <input type="hidden" class="latlng" value="(29.761758, -95.370141)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN AVA - Circle Research, United Kingdom, London">
        <input type="hidden" class="latlng" value="(51.507652, -0.127769)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN AVA - STC Training, United Kingdom, Aberdeen">
        <input type="hidden" class="latlng" value="(57.149785, -2.094408)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN AVA - STC Training, United Arab Emirates, Abu Dhabi">
        <input type="hidden" class="latlng" value="(24.480946, 54.372962)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="BBN AVA - GetIT Comms, Singapore">
        <input type="hidden" class="latlng" value="(1.298999, 103.858908)">
        <input type="hidden" class="kleur" value="oranje">
    </div>
    
    <div class="marker">
        
        <input type="hidden" class="titel" value="">
        <input type="hidden" class="latlng" value="52.233470, 21.001618">
        <input type="hidden" class="kleur" value="paars">
    </div>
    
    <div id="map_canvas"></div>
    <div class="left half twitter">
        <div class="newsfeed twittertop">
            <div class="newsitem">
                <h2>Follow our international partners.</h2>
                
            </div>
        </div>
                <div class="twitterfeed">

            <a class="twitter-timeline" height="500" data-dnt="true"  href="https://twitter.com/BBN_B2B" data-widget-id="492236689729785856" data-chrome="nofooter transparent noheader" data-lang="<?=substr(get_bloginfo( 'language' ), 0, 2);?>">Tweets door @BBN_B2B</a>
          


        </div>
    </div>
    <div class="left half">
        
        <div class="newsfeed">
            
<div class="detailitem">
    
</div>

        </div>
    </div>

                </div>
                <div class="clear"></div>
            </div>

                   <div class="push"></div>
    </div>
</div>
    <script type="text/javascript">!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = "//platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");</script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=<?=substr(get_bloginfo( 'language' ), 0, 2);?>"></script>
    <script type="text/javascript">
        $(function () {
            initialize();
            drawmarkers();
            //makeScrollbar();
        });

        function makeScrollbar()
        {
            $(".twittercontainer").perfectScrollbar();
        }


        function handleTweets(tweets) {
            var x = tweets.length;
            var n = 0;
            var element = document.getElementById('example4');
            var html = '<ul>';
            while (n < x) {
                html += '<li>' + tweets[n] + '</li>';
                n++;
            }
            html += '</ul>';
            element.innerHTML = html;
        }
    </script>
<?php get_footer(); ?>