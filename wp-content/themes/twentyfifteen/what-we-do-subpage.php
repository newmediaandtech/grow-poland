<?php
/**
 *Template Name:What We Do SubPage
 */

get_header(); ?>

<div class="super-container">
    <div class="slider">
        <div class="wrapper mainwrapper">

            <div class="right one-third">
                <?php if ( is_active_sidebar( 'what_we_do' ) ) : ?>
                    <div id="sidebar-left-what-we-do">
                        <?php dynamic_sidebar( 'what_we_do' ); ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="left two-thirds">

                <?php
                // Start the loop.
                while ( have_posts() ) : the_post();?>

                    <article class="entry-content">
                        <?php the_content(); ?>

                    </article>
                <?php endwhile;
                ?>


            </div>


        </div>
        <div class="clear"></div>

        <div class="detailcenter">
<div class="detailitem">
        <div class="news-wrapper" style="width:100%; padding-right: 35%!important; margin-left: 1.1%!important">
            <div class="news-share-container addthiss_bottom">
                <div class="mobile-splitter"></div>
                <div class="addthis_toolbox addthis_default_style" addthis:url="<?the_permalink()?>" addthis:title="<?the_title()?>">
                    <a class="addthis_button_email" target="_blank" title="E-mail" href="#">
                        <span>E-mail</span>
                        <img src="/Content/pictures/share_email_grey.png" alt="e-mail">
                    </a>
                    <a href="<?the_permalink()?>" target="_blank">
                        <span>Permalink</span>
                        <img src="/Content/pictures/share_permalink_grey.png" alt="permalink">
                    </a>
                    <a class="addthis_button_twitter" title="Tweet" href="#">
                        <span>Tweet</span>
                        <img src="/Content/pictures/share_twitter_grey.png" alt="tweet">
                    </a>
                    <a class="addthis_button_facebook" title="Facebook" href="#">
                        <span>Share</span>
                        <img src="/Content/pictures/share_facebook_grey.png" alt="share">
                    </a>
                    <a class="addthis_button_linkedin no-border" target="_blank" title="LinkedIn" href="#">
                        <span>Share</span>
                        <img src="/Content/pictures/share_linkedin_grey.png" alt="share">
                    </a>
                    <div class="atclear"></div></div>
            </div>
        </div>
</div>
</div>
        <div class="push"></div>
    </div>
</div>




<?php get_footer(); ?>
