<?php
/*
*Template Name:News Page
*/

get_header(); 

// $args = array( 'post_type' => 'post', 'category_name' => 'news', 'posts_per_page' => 10, 'orderby' => 'rand');
// $loop = new WP_Query( $args );
?>
<div class="super-container">
        <div class="slider">
            <div class="wrapper mainwrapper">
                
                <div class="left one-third">
                    <?php if ( is_active_sidebar( 'left_news' ) ) : ?>
	                   <div id="sidebar-left-news">
		                  <?php dynamic_sidebar( 'left_news' ); ?>
	                   </div>
                    <?php endif; ?>
                </div>
                <div class="right two-thirds">
    
    <div class="newscontainer">
                    <?php while ( have_posts() ) : the_post(); ?>
                                    <div class="newswrapper">
                                    <div class="newsitem">
                                    <div class="thumb-single">  
                                    <?php twentyfifteen_post_thumbnail(); ?>
                                    </div>
                                    <h2 class="news"><?the_title()?></h2>
                                    <?php the_date('d F Y', '<span class="datum">', '</span>'); ?>
                                    </div> 
                                    <article class="content"><?the_content()?></article>
                                    <div class="interest-fields"><? the_tags('', ', ', '') ?></div>
                                    </div>
                				    
                	<?php endwhile; ?>


                       <?php  the_posts_pagination( array(
                            'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
                            'next_text'          => __( 'Next page', 'twentyfifteen' ),
                            'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
                        ) );
                        ?>
                        </div>
                </div> 

            </div>
                	<div class="clear"></div>
        <div class="push"></div>
    </div>
</div>
<?php get_footer(); ?>