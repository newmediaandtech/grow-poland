<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div class="super-container">
        <div class="slider">
            <div class="wrapper mainwrapper">
                <?php
		// Start the loop.
		while ( have_posts() ) : the_post();?>
                <div class="left one-third">
			<?php get_template_part( 'content', get_post_format() );?>

                </div>
                <div class="right two-thirds">
					<article class="single-portfolio">
					<?=get_field('right_side_images');?>
					</article>
			
				</div>
		<? endwhile; ?>
       	<div class="clear"></div>
        <div class="push"></div>
	</div>
	</div>
	</div>

<?php get_footer(); ?>
